const Joi = require ('joi')

module.exports.verifyUser = function(user){
    const UserSchema = Joi.object().keys({
        username: Joi.string().min(5).max(50),
        name:Joi.string().min(3).max(50),
        password:Joi.string().min(6).max(30),
        role:Joi.string().valid(["ADMIN_ROLE","USER_ROLE"]),
        active:Joi.boolean(),
    })

    const validate =  Joi.validate(user,UserSchema);
    console.log(validate);
    return validate;
}  