const mongoose = require('mongoose');
const { boolean } = require('joi');
const {Schema}=mongoose

const TackingSchema= new Schema({
    status:{type:String,required:true},
    user:{type:String,required:true},
},{
    timestamps:{
        createdAt:true,updatedAt:true
    }
})

module.exports =TackingSchema;
//module.exports= mongoose.model('deliver',TackingSchema);