const mongoose = require('mongoose');
const {Schema}=mongoose;
const {compareSync,hashSync,genSaltSync} = require('bcryptjs');
const {JoiHelper} =require('../helpers');

const ClientSchema= new Schema({
    username:{type:String,required:true, unique:true,min:5,max:50},
    name:{type:String, required:true,min:3,max:50},
    password:{type:String, required:true,min:6},
    active:{type:Boolean,required:true},
    phone:{type:[String],required:true},
    address:{type:[new Schema ({address:{type:String,required:true,minlength:5,maxlength:100},
        city:{type:String,required:true,minlength:3,maxlength:50},country:{type:String}
        ,region:{type:String}})]}
    },{
    timestamps:{
        createdAt:true,updatedAt:true
    }
})

ClientSchema.methods.toJSON= function (){
    let user = this.toObject()
    delete user.password
    return user
}

ClientSchema.pre('save',async function(next){
    const user=this
    
    if (!user.isModified('password')){
        return next()
    }
        const salt = genSaltSync(10)
        const hashedpassword=hashSync(user.password,salt)
        user.password=hashedpassword
        next()
    
})
ClientSchema.methods.comparePasswords =function(password){
    return compareSync(password,this.password)
}

ClientSchema.methods.validateUser =function(user){
   return JoiHelper.verifyUser(user);
}

module.exports= mongoose.model('client',ClientSchema)