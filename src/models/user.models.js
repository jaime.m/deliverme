const mongoose = require('mongoose')
const {Schema}=mongoose
const {compareSync,hashSync,genSaltSync} = require('bcryptjs')
const {JoiHelper} =require('../helpers')

const UserSchema= new Schema({
    username:{type:String,required:true, unique:true,min:5,max:50},
    name:{type:String, required:true,min:3,max:50},
    password:{type:String, required:true,min:6},
    role:{type:String, required:true,enum:["ADMIN_ROLE","USER_ROLE"]},
    active:{type:Boolean,required:true}
},{
    timestamps:{
        createdAt:true,updatedAt:true
    }
})

UserSchema.methods.toJSON= function (){
    let user = this.toObject()
    delete user.password
    return user
}

UserSchema.pre('save',async function(next){
    const user=this
    
    if (!user.isModified('password')){
        return next()
    }
        const salt = genSaltSync(10)
        const hashedpassword=hashSync(user.password,salt)
        user.password=hashedpassword
        next()
    
})
UserSchema.methods.comparePasswords =function(password){
    return compareSync(password,this.password)
}

UserSchema.methods.validateUser =function(user){
   return JoiHelper.verifyUser(user);
}

module.exports= mongoose.model('user',UserSchema)