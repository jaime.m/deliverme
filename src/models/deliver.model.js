const mongoose = require('mongoose')
const {Schema}=mongoose
const trackingSchema =require('./tracking.model')

const DeliverSchema= new Schema({
    username:{type:String,required:true},
    type:{type:String, required:true},
    destination:{type:[String], required:true},
    tracking:{type:[trackingSchema], required:true},
    active:{type:Boolean,required:true}
},{
    timestamps:{
        createdAt:true,updatedAt:true
    }
})

module.exports= mongoose.model('deliver',DeliverSchema)