let _clientService=null
class ClientController{
    constructor({ClientService}){
        _clientService=ClientService
    }

    async get(req,res){
        const {clientId}=req.params;
        const client = await _clientService.get(clientId);
        return res.send(client);
    }
    async getAll(req,res){
        const clients = await _clientService.getAll()
        return res.send(clients)
    }
    async delete(req,res){
        const {clientId}=req.params
        const client = await _clientService.delete(clientId)
        return res.send(client)
    }
    async update(req,res){
        const {body}=req;
        const {clientId}=req.params;
        const client = await _clientService.update(body,clientId)
        return res.send(client)
    }
    async create(req,res){
        const {body}=req;
        const client = await _clientService.create(body)
        return res.send(client)
    }
}
module.exports=ClientController
