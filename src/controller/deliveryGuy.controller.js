let _deliveryGuyService=null
class DeliveryGuyController{
    constructor({ DeliverGuyService}){
        _deliveryGuyService=DeliverGuyService
    }

    async get(req,res){
        const {deliveryGuyId}=req.params;
        const deliveryGuy = await _deliveryGuyService.get(deliveryGuyId);
        return res.send(deliveryGuy);
    }
    async getAll(req,res){
        const deliveryGuys = await _deliveryGuyService.getAll()
        return res.send(deliveryGuys)
    }
    async delete(req,res){
        const {deliveryGuyId}=req.params
        const deliveryGuy = await _deliveryGuyService.delete(deliveryGuyId)
        return res.send(deliveryGuy)
    }
    async update(req,res){
        const {body}=req;
        const {deliveryGuyId}=req.params;
        const deliveryGuy = await _deliveryGuyService.update(body,deliveryGuyId)
        return res.send(deliveryGuy)
    }
    async create(req,res){
        const {body}=req;
        const deliveryGuy = await _deliveryGuyService.create(body)
        return res.send(deliveryGuy)
    }
}
module.exports=DeliveryGuyController
