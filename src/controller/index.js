module.exports={
    UserController:require('./user.controller'),
    AuthController:require('./auth.controller'),
    DeliverController:require('./deliver.controller'),
    DeliverGuyController:require('./deliveryGuy.controller'),
    ClientController:require('./client.controller'),
}