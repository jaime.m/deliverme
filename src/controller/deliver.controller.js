let _deliverService=null
class DeliverController{
    constructor({DeliverService}){
        _deliverService=DeliverService
    }
    async get(req,res){
        const {deliverId}=req.params
        const deliver = await _deliverService.get(deliverId)
        return res.send(deliver)  
    }

    async create(req,res){
        const body=req.body
        const user= req.user;
        body.username= user.user;
        body.tracking=[
            {
                status:"Creado",
                user:user.user,
            }
        ]
        const deliver = await _deliverService.create(body)
        return res.send(deliver)  
    }

    async getAll(req,res){
        const deliver = await _deliverService.getAll()
        return res.send(deliver)
    }

    async delete(req,res){
        const {deliverId}=req.params
        const deliver = await _deliverService.delete(deliverId)
        return res.send(deliver)
    }

    async update(req,res){
        const body=req.body
        const {deliverId}=body
        const deliver = await _deliverService.update(body,deliverId)
        return res.send(deliver)
    }

    async add(req,res){
        const body=req.body
        const {deliverId}=req.params
        body.user=req.user.user;
        const deliver = await _deliverService.add(body,deliverId)
        return res.send(deliver)
    }
    async deleteTracking(req,res){
        const {deliverId,trackingId}=req.params
        const deliver = await _deliverService.deleteTracking(deliverId,trackingId)
        return res.send(deliver)
    }

    
}
module.exports=DeliverController
