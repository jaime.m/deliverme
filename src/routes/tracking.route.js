const {Router}=require('express')
const {AuthMiddleware} = require('../middlewares')

module.exports= function({
      DeliverController
}){
    const router = Router();
    router.patch('/:deliverId',AuthMiddleware,DeliverController.add);
    router.delete('/:deliverId/:trackingId',AuthMiddleware,DeliverController.deleteTracking);
    return router
}