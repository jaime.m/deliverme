const {Router}=require('express')
const {AuthMiddleware} = require('../middlewares')

module.exports= function({
      DeliverGuyController
}){
    const router = Router();
    router.get('/:deliveryGuyId',DeliverGuyController.get);
    router.get('/',DeliverGuyController.getAll);
    router.patch('/:deliveryGuyId',DeliverGuyController.update);
    router.delete('/:deliveryGuyId',DeliverGuyController.delete);
    router.post('/',DeliverGuyController.create);

    return router
}