const {Router}=require('express')
const {AuthMiddleware} = require('../middlewares')

module.exports= function({
      DeliverController
}){
    const router = Router();
    router.get('/:deliverId',AuthMiddleware,DeliverController.get);
    router.post('/',AuthMiddleware,DeliverController.create);
    router.get('/',AuthMiddleware,DeliverController.getAll);
    router.patch('/:deliverId',AuthMiddleware,DeliverController.update);
    router.delete('/:deliverId',AuthMiddleware,DeliverController.delete);


    return router
}