const {Router}=require('express')
const {AuthMiddleware} = require('../middlewares')

module.exports= function({
     ClientController
}){
    const router = Router();
    router.get('/:clientId',ClientController.get);
    router.get('/',ClientController.getAll);
    router.patch('/:clientId',ClientController.update);
    router.delete('/:clientId',ClientController.delete);
    router.post('/',ClientController.create);

    return router
}