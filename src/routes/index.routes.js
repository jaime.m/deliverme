module.exports = {
    UserRoutes:require('./user.routes'),
    DeliverRoutes:require('./deliver.route'),
    AuthRoutes:require('./auth.routes'),
    TrackingRoutes:require('./tracking.route'),
    DeliverGuyRoute:require('./deliveryGuy.route'),
    ClientRoute:require('./client.route'),
}