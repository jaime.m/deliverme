const {Router} = require('express')
const {AuthMiddleware} = require('../middlewares')

module.exports= function({
    AuthController
}){
    const router=Router()
    router.post('/signin',AuthController.singin)
    router.post('/signup',AuthController.singup)
    return router
}
