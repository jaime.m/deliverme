const {Router}=require('express')
const {AuthMiddleware} = require('../middlewares')

module.exports= function({
    UserController
}){
    const router = Router()
    router.get('/:userId',UserController.get)
    router.get('/',UserController.getAll)
    router.patch('/:_id',UserController.update)
    router.delete('/:userId',UserController.delete)
    //Ejemplo de endpoint get!
    router.get('/w/:word',UserController.getW)

    return router
}