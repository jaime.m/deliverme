module.exports={
    UserRepository:require('./user.repository'),
    DeliverRepository:require('./deliver.repository'),
    ClientRepository:require('./client.repository'),
    DeliverGuyRepository:require('./deliverGuy.repository'),
}