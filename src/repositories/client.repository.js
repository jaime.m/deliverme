const BaseRepository= require('./base.repository')
let _client=null

class ClientRepository extends BaseRepository{
    constructor({ClientModel}){
        super(ClientModel)
        _client=ClientModel
    }
    
    async getClientByUsername(username){
        return await _client.findOne({username:username,active:true})
    }
}

module.exports=ClientRepository