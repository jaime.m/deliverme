const BaseRepository= require('./base.repository')
let _deliverGuy=null

class DeliverGuyRepository extends BaseRepository{
    constructor({DeliverGuyModel}){
        super(DeliverGuyModel)
        _deliverGuy=DeliverGuyModel
    }
    
    async getDeliverGuyByUsername(username){
        return await _deliverGuy.findOne({username:username,active:true})
    }
}

module.exports=DeliverGuyRepository