const BaseService = require('./base.service')
let _deliverGuyRepository=null

class deliverGuyService extends BaseService{
    constructor({
      DeliverGuyRepository
    }){
        super(DeliverGuyRepository)
        _deliverGuyRepository=DeliverGuyRepository
    }
    async getDeliverGuyByUsername(username){
        return await _deliverGuyRepository.getDeliverGuyByUsername(username)
    }
}

module.exports=deliverGuyService