const BaseService = require('./base.service')
_deliverRepository = null

class DeliverService extends BaseService{
    constructor({
        DeliverRepository
    }){
        super(DeliverRepository)
        _deliverRepository=DeliverRepository
    }

    async add(body,deliverId){

        const exist = await _deliverRepository.get(deliverId)
        if (!exist) {
            const error = new Error()
            error.status=404
            error.message='Deliver not found'
            throw error
        }

        const deliver=await _deliverRepository.add(exist,body);      
        return deliver;
        
    }

    async deleteTracking(deliverId,trackingId){
        const exist= await _deliverRepository.get(deliverId)
        if(!exist){
            const error = new Error()
            error.status=404
            error.message='Deliver not found'
            throw error
        }

        console.log(exist);
        const deliver=_deliverRepository.deleteTracking(exist,trackingId);      
        return deliver;
    }
}

module.exports=DeliverService