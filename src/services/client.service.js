const BaseService = require('./base.service')
let _clientRepository=null

class ClientService extends BaseService{
    constructor({
     ClientRepository
    }){
        super(ClientRepository)
        _clientRepository=ClientRepository
    }
    async getClientByUsername(username){
        return await _clientRepository.getClientByUsername(username)
    }
}

module.exports=ClientService