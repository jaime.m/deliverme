module.exports = {
    UserService:require('./user.service'),
    AuthService:require('./auth.service'),
    DeliverService:require('./deliver.service'),
    DeliverGuyService:require('./deliveryGuy.service'),
    ClientService:require('./client.service'),
}