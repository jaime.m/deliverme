const {asClass,asFunction,asValue,createContainer} = require('awilix')
//config
const config=require('../config')
const app = require('.')
//Routes
const {UserRoutes,DeliverGuyRoute,ClientRoute,AuthRoutes,DeliverRoutes,TrackingRoutes,} = require('../routes/index.routes')
const Routes = require('../routes')
//Controllers
const { UserController,DeliverGuyController,ClientController,AuthController,DeliverController,}=require('../controller')
//Services
const {UserService,DeliverGuyService,ClientService,AuthService,DeliverService,} = require('../services')
//Repositories
const {UserRepository,DeliverGuyRepository,ClientRepository,DeliverRepository,}= require('../repositories')
//Models
const {UserModel,DeliverGuyModel,ClientModel, DeliverModel, } =require('../models')

const container= createContainer()

container.register({
    router:asFunction(Routes).singleton(),
    app:asClass(app).singleton(),
    config:asValue(config)
})
//Registering the models
.register({
    UserModel:asValue(UserModel),
    DeliverModel:asValue(DeliverModel),
    DeliverGuyModel:asValue(DeliverGuyModel),
    ClientModel:asValue(ClientModel),

})
//Registering the Services
.register({
    UserService:asClass(UserService).singleton(),
    AuthService:asClass(AuthService).singleton(),
    DeliverService:asClass(DeliverService).singleton(),
    DeliverGuyService:asClass(DeliverGuyService).singleton(),
    ClientService:asClass(ClientService).singleton(),
})

//Registering the Repositories
.register({
    UserRepository:asClass(UserRepository).singleton(),
    DeliverRepository:asClass(DeliverRepository).singleton(),
    DeliverGuyRepository:asClass(DeliverGuyRepository).singleton(),
    ClientRepository:asClass(ClientRepository).singleton(),
})
.register({
    UserRoutes:asFunction(UserRoutes).singleton(),
    AuthRoutes:asFunction(AuthRoutes).singleton(),
    DeliverRoutes:asFunction(DeliverRoutes).singleton(),
    TrackingRoutes:asFunction(TrackingRoutes).singleton(),
    DeliverGuyRoutes:asFunction(DeliverGuyRoute).singleton(),
    ClientRoutes:asFunction(ClientRoute).singleton(),
})

.register({
    UserController:asClass(UserController.bind(UserController)).singleton(),
    AuthController:asClass(AuthController.bind(AuthController)).singleton(),
    DeliverController:asClass(DeliverController.bind(DeliverController)).singleton(),
    DeliverGuyController:asClass(DeliverGuyController.bind(DeliverGuyController)).singleton(),
    ClientController:asClass(ClientController.bind(ClientController)).singleton(),
})

module.exports=container